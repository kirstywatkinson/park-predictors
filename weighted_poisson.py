"""
Script used to calculate number of visits per greenspace for a given day
Inputs required:
- Total number of agents in the model
- Choice of weighting scenario 
"""

from scipy.stats import poisson
from numpy import median
from pandas import read_csv,DataFrame
import matplotlib.pyplot as plt
from sys import exit
from math import ceil

def print_out(n, d):
    """convenience function for printing in demo mode"""
    print(f"\n-- {n} --")
    for k, v in d.items():
        print(k, v)
    print()


'''SETTINGS -----------------------------------------------------'''

# what kind of weighting to use:
#   1: un-weighted
#   2: most data = best
#   3: closest to mean = best
#   4: closest to median

SCENARIO = 3

# whether to print out data at each intermediate step (for testing)
DEMO = False

'''DATA INPUT --------------------------------------------------'''

#Read in data used to predict counts
evidence = read_csv('./data/Evidence_counts.csv')
counts = {}

for id, site in evidence.iterrows():
    counts.update({site['Site']: site[1:].tolist()})

# number of people in the model - for GISRUK this is for a typical modelled Monday
n_people = 35654


'''-------------------------------------------------------------'''

# get park labels
parks = counts.keys()

# total count for each greenspace
totals = [sum([counts[p][i] for p in parks]) for i in range(6)]

# average for each greenspace
averages = [sum([counts[p][i] for p in parks])/ len(parks) for i in range(6)]

# median for each greenspace
median = [median([counts[p][i] for p in parks]) for i in range(6)] 

# proportion of people per park, per dataset
proportions = dict()
for k, data in counts.items():
    proportions[k] = [d / t for d, t in zip(data, totals)]
if DEMO:
    print_out("proportions", proportions)

# calculate weightas according to scenario
if SCENARIO == 1:   # un-weighted
    weights = [1]*6                                 

# weight so most data points is best (bias towards mugdock)
elif SCENARIO == 2:
    weights = [(t / sum(totals)) for t in totals]   

# weight so closest to the mean proportion is best
elif SCENARIO == 3:

    # get differences from the mean in each park
    differences = dict()
    for k, v in proportions.items():
        differences[k] = [abs(val - (sum(v) / len(v))) for val in v]

    # then mean difference per dataset (then scale so they sum to 1)
    weights = [sum([differences[p][i] for p in parks]) / len(parks) for i in range(6)]
    #print([m / sum(weights2) for m in weights2])
    weights = [m / sum(weights) for m in weights]
    
elif SCENARIO == 4:

    #get differences from the median in each park
    differences = dict()
    for k, v in counts.items():

        differences[k] = [abs(d - t) for d, t in zip(v, median)]
    
    # then mean difference per dataset (then scale so they sum to 1)
    weights = [sum([differences[p][i] for p in parks]) / len(parks) for i in range(6)]
    weights = [m / sum(weights) for m in weights]


    
else:
    print("Invalid scenario number")
    exit()

# adjust proportion to account for weight then get estimated count
estimated_counts = dict()
for k, v in proportions.items():
    if SCENARIO in [1]:
        # need to divide by length here as it is not already a proportion
        estimated_counts[k] = sum([w * p for w, p in zip(weights, v)]) / len(v) * n_people
    elif SCENARIO in [2, 3,4]:
        estimated_counts[k] = sum([w * p for w, p in zip(weights, v)]) * n_people
if DEMO:
    print_out("estimated_counts", estimated_counts)

#plotting for GISRUK
fig, ax = plt.subplots(figsize=(24, 14))
font = {'family' : 'Arial',
        'size'   : 20}
plt.rc('font', **font)

#set up dictionaries to calculate average of 100 iterations 
averages = {}
avgs = {}

#100 iterations
# estimate number of people in each park with draw from poisson distribution
for k, v in estimated_counts.items():

    iterations = []

    for i in range(100):
        iteration = poisson(v).rvs()
        iterations.append(iteration)

        #plot iteration
        ax.bar(k, iteration ,  facecolor="None", linewidth=0.1, edgecolor="darkgrey")

    
    averages.update({k: ceil(sum(iterations)/len(iterations))})
    avgs.update({k: iterations})

    #when not doing 100 iterations   
    #poissons.update({k:iterations})
        #print(poisson.stats(v, moments='mvsk')) 


#for k,v in poissons.items():

#    print(v)

#plot average of the 100 iterations 
ax.bar(range(len(averages.keys())), averages.values() ,  ls='dashed',facecolor="None", linewidth=1.5, edgecolor="black")


#print(sum(poissons.values())/len(poissons))

#if you want to save one iteration
df = DataFrame.from_dict(averages, orient='index')
df.to_csv('./poissons_greenspaces.csv')

#update axes
ax.set_xticks(range(len(averages)), list(averages.keys()))
ax.set_xlabel('Greenspace', fontsize=20)
ax.set_ylabel('Number of visitors', fontsize=20)
ax.tick_params(axis='both', which='major', labelsize=20)
plt.xticks(rotation=80)

#legend
rects = ax.patches
iter_patch = rects[0]
rects = rects[1500:]

# Make some labels.
labels = [f"{i}" for i in list(averages.values())]

for rect, label in zip(rects, labels):
    height = rect.get_height()
    ax.text(
        rect.get_x() + rect.get_width() / 2, height + 10, label, ha="center", va="bottom"
    
    )


#legend
avg_patch = rects[0]
ax.legend([iter_patch, avg_patch], ["Iteration", "Average of iterations"])

#save figure
fig.savefig('./num_people_sat.png', bbox_inches="tight", dpi=150)
print("done!")



