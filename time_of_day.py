"""
Script used to calculate time of visits for a given day
Inputs required:
- Total number of agents in the model
- Choice of weighting scenario 
"""

import matplotlib.pyplot as plt
from scipy.stats import multinomial, norm
from pandas import read_excel, read_csv, DataFrame
from numpy import unique
from math import ceil

#read in datasets
google = read_excel('./data/pop_times.xlsx', sheet_name=None)
flickr = read_excel('./data/flickr_as_percents.xlsx', sheet_name=None)
survey = read_excel('./data/Questionnaire_time_as_percentages.xlsx', sheet_name=None)


workbooks = [google, flickr, survey]
#monday = tuesday = wednesday = thursday = friday = saturday = sunday = []

#set up dictionary to store averages
avgs = dict()

for k, df in flickr.items():

    #work out average of google data for each greenspace
    #try-except to catch greenspaces without data 
    try:
        flickr_sheet = google[k]
        flickr_sheet['avg'] = flickr_sheet.loc[:, [c for c in flickr_sheet.columns if c!= "Hour"]].mean(axis=1)

    except:

        flickr_sheet = DataFrame()
        flickr_sheet['avg'] = [0]*24

    #work out average of survey data for each greenspace
    #try-except to catch greenspaces without data 
    try:
        survey_sheet = survey[k]
        survey_sheet['avg'] = survey_sheet.loc[:, [c for c in survey_sheet.columns if c!= "Hour"]].mean(axis=1)
    except:
        survey_sheet = DataFrame()
        survey_sheet['avg'] = [0]*24

    df['avg'] = df.loc[:, [c for c in df.columns if c!= "Hour"]].mean(axis=1)

    #work out average of the 3 datasets
    avgs.update({k: [(df['avg']/100).tolist(), (flickr_sheet['avg']/100).tolist(), (survey_sheet['avg']/100).tolist()]})

#set weights equally as 1
weights = [1]*3

#dictionary to store weighted average
weighted_avg = dict()

#iterate through datasets
for k,v in avgs.items():

    #get weighted proportions
    weighted = [w * p for w, p in zip(weights, v)]

    #calculate weighted average of the three datasets
    averages = []
    for i in range(24):
        total = 0
        for w in weighted:
            total =+ w[i]
        averages.append(total/3)

    weighted_avg.update({str(k): averages})

#plotting
fig, ax = plt.subplots(figsize=(17, 11))
font = {'family' : 'Arial',
        'size'   : 20}
plt.rc('font', **font)

#read counts predicted per greenspace
counts = read_csv('./poissons_greenspaces.csv')

# 0-23 (hours)
times = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]

#dictionary to store average of 100 iterations
avg_timings = dict()

#list to store all iterations 
all_times = []

#for each greenspace
for id, site in counts.iterrows():

    #no data
    if(site['Park'] == 'Mains Plantation'):

        continue

    visits = []

    #iteration
    timesInd = []
    for i in range(100):

        #get proportion
        prop = [v for k, v in weighted_avg.items() if site['Park'] in k]
        n_people = site['Number']

        #build multinomial distribution
        dist = multinomial(n_people, prop[0])

        #get hours agents will visit
        times_to_come = dist.rvs()
        
        #add iterations
        iteration = dict()
        timesInd.append(times_to_come[0])
        for i in range(24):
            iteration.update({i: times_to_come[0][i]})
 
        all_times.append({site['Park']: iteration})

    #work out average of 100 iterations for the greenspace
    averages = []
    for i in range(24):
        total = 0
        for time in timesInd:

            total += time[i]

        averages.append(ceil(total/100))

    avg_timings.update({site['Park']:averages})
    

#plot the iterations
for iteration in all_times:

    for k, v in iteration.items():

        #plot kelvingrove for GISRUK
        if k == 'Kelvingrove Park':
            ax.bar(v.keys(), v.values() ,  facecolor="None", linewidth=0.05, edgecolor="grey")

kelvin = avg_timings['Kelvingrove Park']

#plot average for kelvingrove for GISRUK
ax.bar(times, kelvin ,  ls='dashed',facecolor="None", linewidth=1.5, edgecolor="black")

#update axes
plt.xticks(times)
ax.set_xlabel('Hours', fontsize=20)
ax.set_ylabel('Number of visitors', fontsize=20)
ax.tick_params(axis='both', which='major', labelsize=20)

#plt.xticks(rotation=180)

#legend
rects = ax.patches
iter_patch = rects[0]
rects = rects[2400:]

# Make some labels
labels = [f"{i}" for i in kelvin]

#labels for each bar
for rect, label in zip(rects, labels):
    height = rect.get_height()
    ax.text(
        rect.get_x() + rect.get_width() / 2, height+10, label, ha="center", va="bottom"
    )

#legend
avg_patch = rects[0]
ax.legend([iter_patch, avg_patch], ["Iteration", "Average of iterations"])

#save figure
plt.savefig('./hist_sat_kelvin_new.png')
