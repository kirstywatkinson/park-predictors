# Park Predictor

Predict the number of visits to urban greenspace using heterogenous data sources. 

## Description

Scripts used to predict the number of visits to greenspaces using a range of evidence sources.
The weighted_poisson and dayOfWeek scripts can be used to combine data using measures of central tendancy to generate estimated counts per greenspace, which are then used to build a Poisson predictor and predict number of visits per day, per greenspace. 
The time_of_day script combines data using a weighted average to generate estimate visits per greenspace, which are then used to build a multinomial predictor and predict number of visits per hour, per greenspace. 

Used to seed humans in an agent-based model exploring Lyme Disease transmission risk in urban greenspaces. 

![ABM screenshot, humans are shown in yellow](images/ABM_screenshot.png)

## Getting Started

### Dependencies

To execute the script the following libraries are required:
* SciPy (https://scipy.org/)
* Pandas (https://pandas.pydata.org/)
* Numpy (https://numpy.org/)
* Matplotlib (https://matplotlib.org/)


### Installing

* Install Miniconda/Anaconda
* Set up conda environment

```
conda create -n park_predictor -c conda-forge -y python=3 scipy pandas matplotlib numpy
```
* Get Park Predictor
```
git clone git@gitlab.com:kirstywatkinson/park_predictor.git
cd park_predictor
```


### Executing program

* Activate conda environemnt
```
conda activate park_predictor
```
* Run scripts
Run dayOfWeek.py first, and use predicted number of visits for a selected day as an input for weighted_poisson.py. CSV produced by weighted_poisson.py required to run time_of_day.py 

```
python dayOfWeek.py
```
```
python weighted_poisson.py
```
```
python time_of_day.py
```

## Help

Any advise for common problems or issues.
```
command to run if program contains helper info
```

## Authors

Contributors names and contact info

Kirsty Watkinson
(kirsty.watkinson@manchester.ac.uk)

Jonathan Huck
(jonathan.huck@manchester.ac.uk)
 
## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the [NAME HERE] License 

## Acknowledgments
