"""
Script used to calculate number of visits per day
Inputs required:
- Total number of agents in the model
- Choice of weighting scenario 
"""
from scipy.stats import poisson
from numpy import median
from pandas import read_csv,DataFrame
import matplotlib.pyplot as plt
from math import ceil

def print_out(n, d):
    """convenience function for printing in demo mode"""
    print(f"\n-- {n} --")
    for k, v in d.items():
        print(k, v)
    print()


'''SETTINGS -----------------------------------------------------'''

# what kind of weighting to use:
#   1: un-weighted
#   2: most data = best
#   3: closest to mean = best
#   4: closest to median

SCENARIO = 3

# whether to print out data at each intermediate step (for testing)
DEMO = False

'''DATA INPUT --------------------------------------------------'''

# number of people in the model
n_people = 96064

#survey,google,flickr,besttime
# counts = {
#     'Monday':[0.148950575,0.224895833,0.057658024,0.128369272],
#     'Tuesday':[0.144211239,0.234270833,0.05567215,0.119946092],
#     'Wednesday':[0.126607989,0.2265625,0.067136055,0.132749326],
#     'Thursday':[0.129316181,0.250798611,0.190418162,0.141172507],
#     'Friday':[0.117129316,0.254131944,0.140523097,0.144541779],
#     'Saturday':[0.155721056,0.335798611,0.275020874,0.17115903],
#     'Sunday': [0.178063643,0.277743056,0.213571638,0.162061995]
# }

#survey, flickr, besttime
counts = {
    'Monday':[0.148950575,0.057658024,0.128369272],
    'Tuesday':[0.144211239,0.05567215,0.119946092],
    'Wednesday':[0.126607989,0.067136055,0.132749326],
    'Thursday':[0.129316181,0.190418162,0.141172507],
    'Friday':[0.117129316,0.140523097,0.144541779],
    'Saturday':[0.155721056,0.275020874,0.17115903],
    'Sunday': [0.178063643,0.213571638,0.162061995]
}



'''-------------------------------------------------------------'''

# get park labels
days = counts.keys()

# total count for each greenspace
totals = [sum([counts[d][i] for d in days]) for i in range(3)]

#average count for each greenspace
averages = [sum([counts[d][i] for d in days])/ len(days) for i in range(3)]

#median for each greenspace
median = [median([counts[d][i] for d in days]) for i in range(3)]


# calculate weightas according to scenario
if SCENARIO == 1:   # un-weighted
    weights = [1]* len(days)                                

# weight so most data points is best (bias towards greenspace with highest total)
elif SCENARIO == 2:
    weights = [(t / sum(totals)) for t in totals]   

# weight so closest to the mean proportion is best
elif SCENARIO == 3:

    # get differences from the mean in each park
    differences = dict()

    #calculate difference from mean
    for k, v in counts.items():
        differences[k] = [abs(val - (sum(v) / len(v))) for val in v]
    
    # then mean difference per dataset (then scale so they sum to 1)
    weights = [sum([differences[d][i] for d in days]) / len(days) for i in range(3)]
    weights = [m / sum(weights) for m in weights]

# weight so closest to the median proportion is best
elif SCENARIO == 4:

    #calculate difference from median
    differences = dict()
    for k, v in counts.items():
        differences[k] = [abs(d - t) for d, t in zip(v, median)]
    
    # then mean difference per dataset (then scale so they sum to 1)
    weights = [sum([differences[d][i] for d in days]) / len(days) for i in range(3)]
    weights = [m / sum(weights) for m in weights]

else:
    print("Invalid scenario number")
    exit()

# adjust proportion to account for weight then get estimated count
estimated_counts = dict()
for k, v in counts.items():
    if SCENARIO in [1]:
        # need to divide by length here as it is not already a proportion
        estimated_counts[k] = sum([w * p for w, p in zip(weights, v)]) / len(v) * n_people
    elif SCENARIO in [2, 3,4]:
        estimated_counts[k] = sum([w * p for w, p in zip(weights, v)]) * n_people
if DEMO:
    print_out("estimated_counts", estimated_counts)

"""poissons = dict()
# estimate number of people in each park with draw from poisson distribution
# when dont want to plot 
for k, v in estimated_counts.items():

    poissons.update({k:poisson(v).rvs()})
"""

#plotting
fig, ax = plt.subplots(figsize=(24, 14))
font = {'family' : 'Arial',
        'size'   : 20}

plt.rc('font', **font)

#initialise dictionaries to calculate average of 100 iterations 
averages = {}
avgs = {}

#100 iterations
# estimate number of people in each park with draw from poisson distribution
for k, v in estimated_counts.items():

    iterations = []

    #iterate 100 times
    for i in range(100):

        iteration = poisson(v).rvs()
        iterations.append(iteration)

        #plot iteration
        ax.bar(k, iteration ,  facecolor="None", linewidth=0.05, edgecolor="grey")

    #work out average
    averages.update({k: ceil(sum(iterations)/len(iterations))})
    avgs.update({k: iterations})


#for gisruk dont save
df = DataFrame.from_dict(averages, orient='index')
df.to_csv('./days_poisson_new.csv')

#plot average
ax.bar(range(len(averages.keys())), averages.values() ,  ls='dashed',facecolor="None", linewidth=1.5, edgecolor="black")

#update axes
ax.set_xticks(range(len(averages)), list(averages.keys()))
ax.set_xlabel('Day of week',fontsize=20)
ax.set_ylabel('Number of visitors',fontsize=20)
ax.tick_params(axis='both', which='major', labelsize=20)
plt.xticks(rotation=80)

#legend
rects = ax.patches
iter_patch = rects[0]
rects = rects[700:]

# Make some labels.
labels = [f"{i}" for i in list(averages.values())]

#get labels for the bars
for rect, label in zip(rects, labels):
    height = rect.get_height()
    ax.text(
        rect.get_x() + rect.get_width() / 2, height + 10, label, ha="center", va="bottom"
    
    )

#legend
avg_patch = rects[0]

#print(min(averages.values()) - 1000)
ax.set_ylim(bottom=min(averages.values()) - 750, top=max(averages.values()) + 900 )

ax.legend([iter_patch, avg_patch], ["Iteration", "Average of iterations"])

#save figure
fig.savefig('./num_people_week_new.png', bbox_inches="tight")



